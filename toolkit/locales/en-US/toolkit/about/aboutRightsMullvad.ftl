# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

rights-mullvad-intro = { -brand-short-name } is free and open source software.
rights-mullvad-you-should-know = There are a few things you should know:
rights-mullvad-trademarks =
    You are not granted any trademark rights or licenses to the trademarks of
    the { -brand-short-name } or any party, including without limitation the
    { -brand-short-name } name or logo.
